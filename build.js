const fs = require('fs');
const magicImporter = require('node-sass-magic-importer');
const sass = require('node-sass');

require('node-sass').render({
  file: "src/index.scss",
  importer: magicImporter()
},
function(e, result) {
  if (e) {
    console.error(e);
  }
  else {
    if (!fs.existsSync("dist")) {
      fs.mkdirSync("dist");
    }
    fs.writeFileSync("dist/index.css", result.css)
    console.log(result.stats);
  }
});
